var express         = require('express');
var path            = require('path');
var logger          = require('morgan');
var bodyParser      = require('body-parser');
var redis           = require('redis');

var app = express();

var client = redis.createClient();
client.on('connect', function () {
    console.log('redis server connected...');
})

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (request, response) {

    client.lrange('tasks', 0, -1, function (error, reply) {
        client.hgetall('call', function (error, call) {
            response.render('index', {
                title: 'Task List',
                tasks: reply,
                call: call
            });
        })
    });
});

app.post('/task/add', function (request, response) {
    var task = request.body.task;

    client.rpush('tasks', task, function (error, reply) {
        if (error) console.log(error);
        console.log('Task Added ...');
        response.redirect('/');
    });
});

app.post('/task/delete', function (request, response) {
    var tasksToDelele = request.body.tasks;

    client.lrange('tasks', 0, -1, function (error, tasks) {
        for (var i = 0; i < tasks.length; i++) {
            if (tasksToDelele.indexOf(tasks[i]) > -1) {
                client.lrem('tasks', 0, tasks[i], function () {
                    if (error) {
                        console.log(error);
                    }
                });
            }
        }
        response.redirect('/');
    });
});

app.post('/call/add', function (request, response) {
    var newCall = {};

    newCall.name = request.body.name;
    newCall.company = request.body.company;
    newCall.phone = request.body.phone;
    newCall.time = request.body.time;

    client.hmset('call', ['name', newCall.name, 'company', newCall.company, 'phone', newCall.phone, 'time', newCall.time], function (error, reply) {
        if (error) console.log(error);

        console.log(reply);
        response.redirect('/');
    });

});

app.listen(3000);
console.log('Server started on port 3000');

module.exports = app;